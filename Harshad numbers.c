#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    int num,i,sum=0,rem;
    scanf("%d",&num);
    int n = num;
    while(n != 0){
        rem = n% 10;
        sum += rem;
        n /= 10;
    }
    if(num % sum == 0)
        printf("Yes");
    else
        printf("No");

   
    return 0;
}
