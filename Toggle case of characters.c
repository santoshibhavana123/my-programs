#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    char e[100];
    int s,i;
    scanf("%s",e);
    for(i=0;e[i] != '\0';i++){
        if(e[i] >= 65 && e[i] <= 90)
            e[i] = (char)(e[i] + 32);
        else
            e[i] = (char)(e[i] - 32);
    }
    printf("%s",e);
    return 0;
}